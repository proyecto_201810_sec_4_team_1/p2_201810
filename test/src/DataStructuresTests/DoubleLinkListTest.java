package DataStructuresTests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.jupiter.api.Test;

import model.data_structures.DoubleLinkedList;

public class DoubleLinkListTest {

	private static DoubleLinkedList<Integer> dll;

	@Before
	public void setupEscenario1(){
		dll  = new DoubleLinkedList<Integer>();
	}


	public void setupEscenario2(){
		setupEscenario1();
		Integer s1 = new Integer(1);
		Integer s2 = new Integer(2);
		Integer s3 = new Integer(3);
		dll.add(s1);
		dll.add(s2);
		dll.add(s3);
	}
	public void setupEscenario3(){
		setupEscenario2();
		Integer s4 = new Integer(4);
		Integer s5 = new Integer(5);
		dll.add(s4);
		dll.add(s5);
	}

	@Test
	public void testAdd(){
		setupEscenario1();
		assertTrue("La lista inicia mal" , dll.isEmpty());
		setupEscenario2();
		assertEquals("No carg� todos los elementos", 3, dll.size());
		setupEscenario3();
		assertEquals("No carg� todos los elementos", 5, dll.size());
	}

	@Test
	public void get(){
		setupEscenario2();
		assertEquals(dll.get(1), new Integer(1));
		assertEquals(dll.get(new Integer(1)), new Integer(1));
		assertNull("deber�a retornar null",dll.get(5));
		setupEscenario3();
		assertTrue("Deber�a encontrar el elemento",dll.get(5) != null && dll.get(new Integer(5)) != null);

	}

	@Test
	public void testDel(){
		setupEscenario2();
		//		dll.delete(element)
		assertEquals("No carg� todos los elementos", 3, dll.size());
		assertTrue("est� eliminando mal",dll.delete(new Integer(2)));
		assertEquals("No reduce el size al eliminar", 2, dll.size());
		assertTrue("No deber�a haber eliminado nada",!dll.delete(new Integer(2)));
		assertEquals("Est� eliminando algo que no existe o resizeando mal", 2, dll.size());
	}

}
