package DataStructuresTests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.jupiter.api.Test;

import model.data_structures.RedBlackBST;

public class BalancedTreeTest {

	private RedBlackBST<Integer> rbt;

	@Before
	public void setupEscenario1(){
		rbt  = new RedBlackBST<Integer>();
	}


	public void setupEscenario2(){
		setupEscenario1();
		Integer s1 = new Integer(1);
		Integer s2 = new Integer(2);
		Integer s3 = new Integer(3);
		rbt.put(s1);
		rbt.put(s2);
		rbt.put(s3);
	}
	public void setupEscenario3(){
		setupEscenario2();
		Integer s4 = new Integer(4);
		Integer s5 = new Integer(5);
		rbt.put(s4);
		rbt.put(s5);
	}

	@Test
	public void testAdd(){
		setupEscenario1();
		assertTrue("La lista inicia mal" , rbt.isEmpty());
		setupEscenario2();
		assertEquals("No carg� todos los elementos", 3, rbt.size());
		setupEscenario3();
		assertEquals("No carg� todos los elementos", 5, rbt.size());
	}

	@Test
	public void get(){
		setupEscenario2();
		assertEquals(rbt.get(1), new Integer(1));
		assertEquals(rbt.get(new Integer(1)), new Integer(1));
		assertNull("deber�a retornar null",rbt.get(5));
		setupEscenario3();
		assertTrue("Deber�a encontrar el elemento",rbt.get(5) != null && rbt.get(new Integer(5)) != null);

	}

	@Test
	public void testDel(){
		setupEscenario2();
		//		dll.delete(element)
		assertEquals("No carg� todos los elementos", 3, rbt.size());
		rbt.delete(new Integer(2));
		assertEquals("est� eliminando mal",true,rbt.size() == 2);
		rbt.delete(new Integer(2));
		assertEquals("No deber�a haber eliminado nada",2,rbt.size());
		assertEquals("Est� eliminando algo que no existe o resizeando mal", 2, rbt.size());
	}
}
