package model.logic;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

import model.data_structures.IList;
import model.data_structures.OrderedHeap;
import model.data_structures.Queue;
import model.data_structures.RedBlackBST;
import model.data_structures.SeparateChainingHT;
import model.value_objects.*;

public class Project2Manager {

	//__________ CONSTANTES PARA LA CARGA DE LOS ARCHIVOS_____________________
	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	public static final String DIRECCION_LARGE_JSON = "large"; //direccion estandar para acoplar a los viewers gen�ricos
	public static final String DIRECCION_LARGE= "./data/Large/taxi-trips-wrvz-psew-subset-0";
	public static final String COMPLEMENT_DIR = "-02-2017.json";
	public static final String DIRECCION_TEST_JSON = "./data/Test.json";
	public static final int CANTIDAD_ARCHIVOS = 7; //cantidad de archivos para la carga fragmentada del JSON large

	// ATRIBUTOS QUE REPRESENTAN LOS CONJUNTOS DE DATOS PARA CADA REQUERIMIENTO
	private double longitudPromedio = 0, latitudPromedio = 0; private int cantServicios = 0;

	//	private RedBlackBST<Servicio> servicios = new RedBlackBST<Servicio>();

	private RedBlackBST<RangoDistancia> forB1 = new RedBlackBST<>();
	private SeparateChainingHT<String, zonaXY> forB2 = new SeparateChainingHT<>();

	private RedBlackBST<node> forC1 = new RedBlackBST<>();

	private RedBlackBST<TaxiConServicios> taxisForC2 = new RedBlackBST<>();
	private SeparateChainingHT<String, RangoDistancia> serviciosEnRango = new SeparateChainingHT<>(50);

	private RedBlackBST<Rango15Min> forC3 = new RedBlackBST<>();

	// METODOS PARA LOS REQUERIMIENTOS

	public IList<TaxiConServicios> A1TaxiConMasServiciosEnZonaParaCompania(int zonaInicio, String compania) {

		return new Queue<TaxiConServicios>();
	}

	public IList<Servicio> A2ServiciosPorDuracion(int duracion) {
		return new Queue<Servicio>();
	}

	/**
	 *  Requerimiento 1B
	 */
	public Queue<Servicio> B1ServiciosPorDistancia(double distanciaMinima, double distanciaMaxima) {
		Queue<RangoDistancia> a = (Queue<RangoDistancia>)forB1.keys();
		Queue<Servicio> toRet = new Queue<>();
		for (RangoDistancia rd : a)if(rd.isRange(distanciaMinima, distanciaMaxima))rd.getServices(toRet);
		return toRet;
	}

	/**
	 *  Requerimiento 2B
	 */
	public Queue<Servicio> B2ServiciosPorZonaRecogidaYLlegada(int zonaInicio, int zonaFinal, String fechaI, String fechaF, String horaI, String horaF) {
		RangoFechaHora r = new RangoFechaHora(fechaI, fechaF, horaI, horaF);
		Queue<Servicio> srvcs = new Queue<Servicio>();
		Queue<Servicio> toRet = new Queue<>();
		zonaXY temp = forB2.get(zonaInicio + "-" + zonaFinal);
		if(temp != null)temp.getServices(srvcs);
		for (Servicio servicio : srvcs)	if(servicio.estaEnRango(r))toRet.add(servicio);
		return toRet;
	}

	/**
	 *  Requerimiento 1C
	 */
	public TaxiConPuntos[] R1C_OrdenarTaxisPorPuntos() {
		TaxiConPuntos[] toRet = new TaxiConPuntos[10];
		OrderedHeap<TaxiConPuntos> top = new OrderedHeap<>(6000); //taxis ordenados por puntos
		//a�ade los nodos con taxis
		Iterable<node> iter = forC1.keys();	for (node n : iter) top.insert(n.taxi);
		int i;
		if(top.size()<= 10) for (i = top.size()-1; i >= 0; i++)toRet[i] = top.remove();
		else {
			for (i=9; i>=5; i--) toRet[i]= top.remove();
			while(top.size() >5)		   top.remove();
			for (i=4; i>=0; i--) toRet[i]= top.remove();
		}
		return toRet;
	}

	/**
	 *  Requerimiento 2C
	 */
	public Queue<Servicio> R2C_LocalizacionesGeograficas(String taxiIDReq2C, double millasReq2C) {
		TaxiConServicios t = new TaxiConServicios(taxiIDReq2C, null);
		TaxiConServicios elTaxiBuscadoEs = taxisForC2.get(t);
		Queue<Servicio> temp = new Queue<>();
		if(elTaxiBuscadoEs != null)for (Servicio s : elTaxiBuscadoEs.getServicios()) {
			double distance = s.getDistance(latitudPromedio, longitudPromedio);
			System.out.println(distance);
			if(distance < millasReq2C) {
				temp.add(s);
			}
		}
		return temp;
	}

	/**
	 *  Requerimiento 3C
	 *  Se tiene un �rbol binario balanceado con los servicios registrados en el sistema, ordenados
	 *  por fecha y hora (en rangos de 15 minutos, empezando por la hora exacta con 00 minutos)).
	 *  Dada una fecha y hora (con un n�mero arbitrario de minutos en el rango [0, 59]) se debe retornar
	 *  todos los servicios registrados en el rango de 15 minutos m�s cercano, que adicionalmente hayan
	 *  salido de una zona y hayan terminado en otra zona. 
	 */
	public Queue<Servicio> R3C_ServiciosEn15Minutos(String fecha, String hora) {
		String[] horaMins = hora.split(":");
		String horaTemp = horaMins[0] + ":";
		String fechaResult = fecha + "T";
		if(Integer.parseInt(horaMins[1]) < 8)fechaResult += horaTemp + "00" + ":00.000";
		else if(Integer.parseInt(horaMins[1]) < 23)fechaResult += horaTemp + "15" + ":00.000";
		else if(Integer.parseInt(horaMins[1]) < 38)fechaResult += horaTemp + "30" + ":00.000";
		else if(Integer.parseInt(horaMins[1]) < 53)fechaResult += horaTemp + "45" + ":00.000";
		else {
			int h = Integer.parseInt(horaMins[0]);
			if(h == 23) {
				String[] day = fecha.split("-");
				int temp = Integer.parseInt(day[2]);
				day[2] ="0"+ ++temp;
				fechaResult = day[0] + "-" + day[1] + "-" + day[2] + "T" + "00:00:00.000";
			}
			else if(h < 9) { h++; horaMins[0] = "0"+h;}
			else horaMins[0] = "" + ++h;
			fechaResult += horaMins[0] + ":00:00.000";
		}
		Rango15Min r = forC3.get(new Rango15Min(fechaResult));
		return (r != null)?(Queue<Servicio>) r.servicios():new Queue<>();
	}

	//------------------INICIO DE LOS METODOS DE CARGA------------------------
	public void load(String ruta){	Gson g = new GsonBuilder().create();
	if(!ruta.equals(DIRECCION_LARGE_JSON)) cargaSML(g, ruta);
	else for(int i = 2; i<CANTIDAD_ARCHIVOS+2;i++) cargaSML(g, DIRECCION_LARGE + i + COMPLEMENT_DIR);}

	private void cargaSML(Gson g, String dirJson){
		try{FileInputStream stream = new FileInputStream(new File(dirJson));
		JsonReader reader = new JsonReader(new InputStreamReader(stream, "UTF-8")); reader.beginArray();

		while (reader.hasNext()) {Servicio srvc = g.fromJson(reader, Servicio.class);
		//carga un conjunto de servicios - temporalmente desabilitado por eficiencia
		cantServicios ++; latitudPromedio += srvc.getPickupLatitud(); longitudPromedio += srvc.getPickupLongitud();

		//carga para req 1B
		if(srvc.tieneDistancia()) {
			RangoDistancia rangoTemp = new RangoDistancia(srvc.getTripMiles());
			RangoDistancia r = forB1.get(rangoTemp);
			if(r != null)r.insertService(srvc);
			else {rangoTemp.insertService(srvc); forB1.put(rangoTemp);}}

		//carga para req2B
		zonaXY zonaTemp = forB2.get(srvc.getXYZone());
		if(zonaTemp != null)	zonaTemp.insertService(srvc);
		else {zonaTemp = new zonaXY(srvc.getXYZone()); zonaTemp.insertService(srvc); forB2.put(srvc.getXYZone(), zonaTemp);}

		//carga para req1C
		if(srvc.getTripMiles() > 0 && srvc.getTripTotal() > 0) {
			node n = new node(srvc.getTaxiId());	node temp = forC1.get(n);
			if(temp != null)temp.taxi.addService(srvc.getTripTotal(), srvc.getTripMiles());
			else {n.taxi.addService(srvc.getTripTotal(), srvc.getTripMiles()); forC1.put(n);}
		}

		//carga para req2C
		TaxiConServicios tcsrvc = new TaxiConServicios(srvc.getTaxiId(), srvc.getCompany());
		TaxiConServicios taxiTemp = taxisForC2.get(tcsrvc);
		if(taxiTemp != null)taxiTemp.agregarServicio(srvc);
		else {tcsrvc.agregarServicio(srvc);taxisForC2.put(tcsrvc);}

		//carga para req3C
		if((srvc.getPickArea() != srvc.getDropArea()) && srvc.getTripStart() != null ) {
			Rango15Min r = new Rango15Min(srvc.getTripStart());
			Rango15Min rangoTemp = forC3.get(r);
			if(rangoTemp != null)rangoTemp.addService(srvc);
			else {r.addService(srvc); forC3.put(r);}
		}
		}
		reader.close(); System.out.println("hay en total: "+cantServicios+" servicios");
		}catch(Exception e){System.out.println("Error en la carga del los archivos");e.printStackTrace();}
		if(cantServicios != 0)latitudPromedio = latitudPromedio / cantServicios; longitudPromedio = longitudPromedio / cantServicios;
	}

	private class node implements Comparable<node>{
		private String id;	private TaxiConPuntos taxi;
		public node(String Id) {taxi = new TaxiConPuntos(Id); this.id = Id;}
		@Override public int compareTo(node arg0) {return this.id.compareTo(arg0.id);}
	}
}
