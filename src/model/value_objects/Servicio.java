package model.value_objects;

/**
 * Representation of a Service object
 */
public class Servicio implements Comparable<Servicio>{	

	private String trip_id, taxi_id, company;
	//	______________________
	private String trip_start_timestamp, trip_end_timestamp;
	private int dropoff_community_area, pickup_community_area, trip_seconds;
	private double trip_miles, trip_total, pickup_centroid_latitude , pickup_centroid_longitude;

	public int getPickArea(){return pickup_community_area;}
	public int getDropArea(){return dropoff_community_area;}

	// point: dropoff_centroid_location, pickup_centroid_location
	//	 dropoff_census_tract
	//	extras, fare, payment_type, pickup_census_tract, dropoff_centroid_latitude
	//	dropoff_centroid_longitude, tips, tolls

	/**
	 * @return id - Trip_id
	 */
	public String getTripId(){return trip_id;}	

	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {return taxi_id;}	

	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {return trip_seconds;}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {return trip_miles;}

	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {return trip_total;}

	public double getPickupLatitud() {return pickup_centroid_latitude;}

	public double getPickupLongitud() {return pickup_centroid_longitude;}

	public String getTripStart() {return trip_start_timestamp;}

	public String getTripEnd() {return trip_end_timestamp;}

	public String getCompany(){return company;}

	public String getXYZone() {return pickup_community_area + "-" + dropoff_community_area;}

	public boolean hasCompany() {return (this.company != null && this.company != "");}

	public boolean tieneDistancia() {return this.trip_miles != 0.0;}

	public boolean estaEnRango(RangoFechaHora rango){
		boolean estaIn = false, estaOut = false;
		String fechaIn = rango.getFechaInicial();	String fechaFn = rango.getFechaFinal();
		if(trip_start_timestamp != null && trip_start_timestamp.compareTo(fechaIn) >= 0)estaIn = true;
		if(trip_end_timestamp != null && (trip_end_timestamp.compareTo(fechaFn)<=0))estaOut = true;
		return (estaIn && estaOut);}

	@Override
	public int compareTo(Servicio arg0) {
		return (this.trip_id.compareTo(arg0.getTripId())<0)?-1:
			(this.trip_id.compareTo(arg0.getTripId())>0)?1:0;}

	public boolean equals(Servicio srvc) {
		if(this == srvc)return true;
		if(srvc == null)return false;
		if(srvc.getClass() != this.getClass())return false;

		if(srvc.getTripId() != this.trip_id)return false;
		return true;
	}

	public double getDistance (double lat2, double lon2)  {
		// TODO Auto-generated method stub
		double radioTierra = 3958.75;//en millas  
		double dLat = Math.toRadians(lat2 - this.pickup_centroid_latitude);  
		double dLng = Math.toRadians(lon2 - this.pickup_centroid_longitude);  
		double sindLat = Math.sin(dLat / 2);  
		double sindLng = Math.sin(dLng / 2);  
		double va1 = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)  
		* Math.cos(Math.toRadians(this.pickup_centroid_latitude)) * Math.cos(Math.toRadians(lat2));  
		double va2 = 2 * Math.atan2(Math.sqrt(va1), Math.sqrt(1 - va1));  
		double distancia = radioTierra * va2;  

		System.out.println(distancia);
		return distancia;
	} 

	@Override
	public int hashCode() {
		return trip_id.hashCode();
	}
}
