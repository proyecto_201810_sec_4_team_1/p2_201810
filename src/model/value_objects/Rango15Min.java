package model.value_objects;

import model.data_structures.Queue;

public class Rango15Min implements Comparable<Rango15Min>{

	private String fechahoraInicio;

	private Queue<Servicio> servicios;

	public Rango15Min(String date) {this.fechahoraInicio = date; servicios = new Queue<>();}

	public void addService(Servicio srvc) {servicios.add(srvc);}

	public Iterable<Servicio> servicios(){
		return servicios;
	}

	@Override public int compareTo(Rango15Min arg0) {return this.fechahoraInicio.compareTo(arg0.fechahoraInicio);}
}
