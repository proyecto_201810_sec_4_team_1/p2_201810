package model.value_objects;

import model.data_structures.Queue;
import model.data_structures.RedBlackBST;

public class Compania implements Comparable<Compania> {

	private String nombre;

	private RedBlackBST<TaxiConServicios> taxisInscritos;	

	public Compania(String nombre){this.nombre = nombre;
	taxisInscritos = new RedBlackBST<>();}

	public String getNombre() {return nombre;}

	public void setNombre(String nombre) {this.nombre = nombre;}

	public Queue<TaxiConServicios> getTaxisInscritos() {return (Queue<TaxiConServicios>) taxisInscritos.keys();}

	@Override
	public int compareTo(Compania o) {
		return (this.nombre != null)?this.nombre.compareTo(o.getNombre()):0;
	}

	public void addTaxi(TaxiConServicios t) {
		this.taxisInscritos.put(t);
	}
}
