package model.value_objects;

import model.data_structures.Queue;
import model.data_structures.RedBlackBST;

public class TaxiConServicios implements Comparable<TaxiConServicios>{

	private String taxiId;
	private String compania;
	private RedBlackBST<Servicio> servicios;

	public TaxiConServicios(String taxiId, String compania){
		this.servicios = new RedBlackBST<>();
		this.taxiId = taxiId;
		this.compania = (compania!= null)?compania:"Independent Owner";		
	}

	public String getTaxiId() {
		return taxiId;
	}

	public String getCompania() {
		return compania;
	}

	public Queue<Servicio> getServicios()
	{
		return (Queue<Servicio>)servicios.keys();
	}

	public int numeroServicios(){
		return servicios.size();
	}

	public void agregarServicio(Servicio servicio){
		servicios.put(servicio);
	}

	public boolean equals(TaxiConServicios taxi) {
		if(this == taxi)return true;
		if(taxi == null)return false;
		if(taxi.getClass() != this.getClass())return false;

		if(taxi.getTaxiId() != this.taxiId)return false;
		return true;
	}

	@Override
	public int compareTo(TaxiConServicios o) {
		return this.taxiId.compareTo( o.getTaxiId());
	}

	public void print(double a, double b){
		System.out.println(Integer.toString(numeroServicios())+" servicios "+" Taxi: "+taxiId);
		for(Servicio s : servicios.keys()){
			System.out.println(s.getDistance(a, b));
		}
		System.out.println("___________________________________");;
	}

	public void print(){
		System.out.println(Integer.toString(numeroServicios())+" servicios "+" Taxi: "+taxiId);
		for(Servicio s : servicios.keys()){
			System.out.println("\t"+s.getTripStart());
		}
		System.out.println("___________________________________");;
	}
}
