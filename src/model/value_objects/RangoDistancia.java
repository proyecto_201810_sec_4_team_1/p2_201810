package model.value_objects;

import model.data_structures.Queue;
import model.data_structures.RedBlackBST;

public class RangoDistancia implements Comparable<RangoDistancia>{

	private double distancia;

	private RedBlackBST<Servicio> serviciosEnRango;

	public RangoDistancia(double distance) {
		if (distance == 0.0)throw new IllegalArgumentException("No es valido un grupo con distancia 0.0");
		this.distancia = distance;
		serviciosEnRango = new RedBlackBST<>();
	}

	public double distance() {return distancia;}
	public void insertService(Servicio service) {if (service != null) serviciosEnRango.put(service);}
	public boolean isRange(double minDistance, double maxDistance) {return this.distancia <= maxDistance && this.distancia >= minDistance; }

	@Override
	public int compareTo(RangoDistancia r) {
		if(r == null)throw new IllegalArgumentException("No puedo comparar null !!!");
		return (this.distancia < r.distancia)?-1:(this.distancia == r.distancia)?0:1;
	}
	public void getServices(Queue<Servicio> toRet) {
		Queue<Servicio> vals = (Queue<Servicio>) serviciosEnRango.keys();
		for (Servicio srvc: vals) toRet.add(srvc);
	}

}
