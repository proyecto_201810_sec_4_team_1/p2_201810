package model.value_objects;

public class TaxiConPuntos implements Comparable<TaxiConPuntos>{

	private double tripsTotal, tripsMiles;
	private String taxiId;
	private int cantidadDeServicios;

	public TaxiConPuntos(String id) {this.taxiId = id; tripsMiles = tripsTotal = cantidadDeServicios = 0;}

	public void addService(double total, double miles) {
		tripsTotal += total;
		tripsMiles += miles;
		cantidadDeServicios++;
	}

	public String taxiId() {return this.taxiId;}
	/**
	 * @return puntos - puntos de un Taxi
	 */
	public double getPuntos(){
		return (tripsTotal / tripsMiles) * cantidadDeServicios;
	}

	@Override
	public int compareTo(TaxiConPuntos t) {
		return (this.getPuntos() > t.getPuntos())?1:(this.getPuntos() < t.getPuntos())?-1:0;
	}
}

